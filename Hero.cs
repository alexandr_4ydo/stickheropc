﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace StickHero
{
    class Hero
    {
        Ellipse m_heroEllipse;
        Ellipse m_herosoulEllipse;
        Canvas m_view;

        const double m_WidthAndHeight = 40;
        const double m_ground = 411;
        const double m_underGround = 550;
        double m_heroPositionTop = 300;
        double m_heroPositionLeft = 34;
        double m_soulPositionTop = 305;
        double m_soulPositionLeft = 50;
        bool m_onTheBridge;

        public Hero(Canvas View)
        {
            m_heroEllipse = new Ellipse();
            m_herosoulEllipse = new Ellipse();
            m_view = View;
            m_heroEllipse.Width = m_WidthAndHeight;
            m_heroEllipse.Height = m_WidthAndHeight;
            m_herosoulEllipse.Width = 5;
            m_herosoulEllipse.Height = 5;
            Draw();
        }

        #region Properties
        public bool OnTheBridge
        {
            get { return m_onTheBridge; }
        }
        public bool Landed { get; set; }
        #endregion

        #region Methods
        private void Draw()
        {
            m_heroEllipse.Fill = new SolidColorBrush(Colors.Black);
            m_herosoulEllipse.Fill = new SolidColorBrush(Colors.White);
            m_heroEllipse.StrokeThickness = 1;
            Canvas.SetTop(m_heroEllipse, m_heroPositionTop);
            Canvas.SetLeft(m_heroEllipse, m_heroPositionLeft);
            m_view.Children.Add(m_heroEllipse);
            Canvas.SetTop(m_herosoulEllipse, m_soulPositionTop);
            Canvas.SetLeft(m_herosoulEllipse, m_soulPositionLeft);
            m_view.Children.Add(m_herosoulEllipse);
        }

        public void MoveToWin(double IslandRight, double BridgeRight)
        {
            m_view.Dispatcher.BeginInvoke(new Action(() => JumpUp()));
                do
                {
                        Move();
                    if (m_soulPositionLeft == BridgeRight)
                        m_view.Dispatcher.BeginInvoke(new Action(() => JumpDown()));
                } while (m_heroPositionLeft + m_WidthAndHeight != IslandRight);
        }

        public void MoveToLose(double BridgeRight)
        {
            m_view.Dispatcher.BeginInvoke(new Action(() => JumpUp()));
            do
            {
                Move();
            } while (m_heroPositionLeft + m_WidthAndHeight / 2 != BridgeRight);
            LostDropDown();
        }

        private void Move()
        {
            Thread.Sleep(2);

            m_heroPositionLeft++;
            m_soulPositionLeft++;

            m_view.Dispatcher.BeginInvoke(new Action(() =>
            {
                Canvas.SetLeft(m_heroEllipse, m_heroPositionLeft);
                Canvas.SetLeft(m_herosoulEllipse, m_soulPositionLeft);
            }));
        }

        private void JumpUp()
        {
            Canvas.SetTop(m_heroEllipse, m_heroPositionTop - 6);
            Canvas.SetTop(m_herosoulEllipse, m_soulPositionTop - 6);
            m_onTheBridge = true;
        }

        public void JumpDown()
        {
            Canvas.SetTop(m_heroEllipse, m_heroPositionTop);
            Canvas.SetTop(m_herosoulEllipse, m_soulPositionTop);
            m_onTheBridge = false;
        }

        void DropDown()
        {
            Thread.Sleep(2);
            m_heroPositionTop++;
            m_soulPositionTop++;

                m_view.Dispatcher.BeginInvoke(new Action(() =>
                {
                    Canvas.SetTop(m_heroEllipse, m_heroPositionTop);
                    Canvas.SetTop(m_herosoulEllipse, m_soulPositionTop);
                }));
        }

        void LostDropDown()
        {
            do
            {
                DropDown();
            }
            while (m_heroPositionTop != m_underGround);          
        }

        public void Land()
        {
            do
            {
                DropDown();
            }
            while (m_heroPositionTop != m_ground);
            Landed = true;
        }

        public void MoveBack()
        {
            Canvas.SetLeft(m_heroEllipse, m_heroPositionLeft--);
            Canvas.SetLeft(m_herosoulEllipse, m_soulPositionLeft--);
        }

        #endregion
    }
}
