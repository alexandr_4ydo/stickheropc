﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Threading;

namespace StickHero
{
    class Islands
    {
        const double m_islandsY = 450;
        const double m_islandsHeight = 120;
        const double m_maxIslandWidth = 80;
        const double m_bonusRectWidth = 10;
        double m_firstIslandWidth = 80;
        double m_secondIslandwidth;
        double m_firstIslandLeft = 0;
        private double m_secondIslandLeft, m_bonusRectLeft;

        Random m_random = new Random();
        Canvas m_view;
        Rectangle m_firstIslandRect, m_secondIslandRect, m_bonusRect;

        public Islands(Canvas View)
        {
            m_view = View;
            DrawSecondIsland();
            DrawFirstIsland();
        }

        public Islands(Canvas View, double FirstIslandsWidth)
        {
            m_firstIslandWidth = FirstIslandsWidth;
            m_firstIslandLeft = m_maxIslandWidth - FirstIslandsWidth;
            m_view = View;
            DrawFirstIsland();
            DrawSecondIsland();
        }

        #region properties
        public double BonusRectRight
        {
            get { return m_bonusRectLeft + m_bonusRectWidth; }
        }

        public double BonusRectLeft
        {
            get { return m_bonusRectLeft; }
        }

        public double SecondIslandLeft
        {
            get { return m_secondIslandLeft; }
        }

        public double SecondIslandRight
        {
            get { return SecondIslandLeft + m_secondIslandwidth; }
        }

        public bool OnStartPosition
        {
            get { return m_secondIslandLeft + m_secondIslandwidth != m_maxIslandWidth; }
        }

        public double SecondIslandWidth
        {
            get { return m_secondIslandRect.Width; }
        }

        #endregion

        #region methods

        public void DrawSecondIsland()
        {
            m_secondIslandRect = new Rectangle();
            m_secondIslandwidth = m_random.Next(20, (int)m_firstIslandWidth);
            m_secondIslandLeft = m_random.Next(120, 390);
            m_secondIslandRect.Width = m_secondIslandwidth;
            m_secondIslandRect.Height = m_islandsHeight;
            m_secondIslandRect.Fill = new SolidColorBrush(Colors.Black);
            m_secondIslandRect.StrokeThickness = 1;
            Canvas.SetTop(m_secondIslandRect, m_islandsY);
            Canvas.SetLeft(m_secondIslandRect, m_secondIslandLeft);
            m_view.Children.Add(m_secondIslandRect);
            DrawBonusRect();
        }

        void DrawFirstIsland()
        {
            m_firstIslandRect = new Rectangle();
            m_firstIslandRect.Width = m_firstIslandWidth;
            m_firstIslandRect.Height = m_islandsHeight;
            m_firstIslandRect.Fill = new SolidColorBrush(Colors.Black);
            m_firstIslandRect.StrokeThickness = 1;
            Canvas.SetTop(m_firstIslandRect, m_islandsY);
            Canvas.SetLeft(m_firstIslandRect, m_firstIslandLeft);
            m_view.Children.Add(m_firstIslandRect);
        }

        void DrawBonusRect()
        {
            m_bonusRect = new Rectangle();
            m_bonusRect.Width = m_bonusRectWidth;
            m_bonusRect.Height = m_bonusRectWidth;
            m_bonusRect.Fill = new SolidColorBrush(Colors.Red);
            m_bonusRect.StrokeThickness = 1;
            Canvas.SetTop(m_bonusRect, m_islandsY);
            Canvas.SetLeft(m_bonusRect, GetBonusRectPosition());
            m_view.Children.Add(m_bonusRect);
        }

        double GetBonusRectPosition()
        {
            m_bonusRectLeft = (m_secondIslandwidth / 2 - m_bonusRectWidth / 2) + m_secondIslandLeft;
            return m_bonusRectLeft;
        }

        public void RemoveShapes()
        {
            m_view.Children.Remove(m_secondIslandRect);
            m_view.Children.Remove(m_firstIslandRect);
            m_view.Children.Remove(m_bonusRect);
        }

        public void MoveBack()
        {
            Canvas.SetLeft(m_secondIslandRect, m_secondIslandLeft--);
            Canvas.SetLeft(m_bonusRect, m_bonusRectLeft--);
            Canvas.SetLeft(m_firstIslandRect, m_firstIslandLeft--);
        }
        #endregion
    }
}
