﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;
using System.Windows;

namespace StickHero
{
    enum StatusGame { Win, Loss }

    class Controller 
    {
        public event Action<int, int> ScoreChanged = delegate(int Score, int BestScore) { };
        List<Task> m_tasks = new List<Task>();

        Canvas m_view;
        Islands m_island;
        Hero m_hero;
        Bridge m_bridge;

        StatusGame GameStatus;
        bool m_platformHit;
        bool m_closing = false;
        int m_score = 0, m_bestScore = 0;

        public Controller(Canvas View)
        {
            m_view = View;
            Start(View);
        }

        private void Start(Canvas View)
        {  
            m_island = new Islands(m_view);
            m_hero = new Hero(m_view);
        }

        #region Methods
        public void OnStart()
        {          
            Task taskLand = new Task(m_hero.Land);
            m_tasks.Add(taskLand);
            taskLand.Start();
        }

        public void OnMouseDown()
        {
            if (m_bridge == null && m_hero.Landed)
            {
                m_bridge = new Bridge(m_view);
                Task taskPlay = new Task(Play);
                taskPlay.Start();
                m_tasks.Add(taskPlay);
            }
        }

        public void OnMouseUp()
        {
            if (m_hero.Landed && m_bridge!= null)
            {
                m_bridge.StopGrowth();              
            }
        }

        void Play()
        {
            m_bridge.StartGrow();
            m_bridge.Lower();
            m_platformHit = m_bridge.Hit(m_island.SecondIslandLeft, m_island.SecondIslandRight);
       
            if(m_platformHit) 
            {
                m_hero.MoveToWin(m_island.SecondIslandRight,m_bridge.Lenght);
                GameStatus = StatusGame.Win;
            } 
            else
            {
                m_hero.MoveToLose(m_bridge.Lenght);
                GameStatus = StatusGame.Loss;
            }
            GenerateNextLevel();
        }

        public void GenerateNextLevel()
        {            
            if (GameStatus == StatusGame.Win)
            {
                if (m_bridge.Hit(m_island.BonusRectLeft, m_island.BonusRectRight))
                {
                    m_score+=2;
                }
                else 
                    m_score++;
                if (m_bestScore < m_score)
                    m_bestScore = m_score;
                ScoreChanged(m_score, m_bestScore);
                ToStartPositions();
            }
            else if (GameStatus == StatusGame.Loss)
            {
                m_score = 0;
                ScoreChanged(m_score, m_bestScore);
                Restart();
            }
        }

        private void Restart()
        {
            m_view.Dispatcher.BeginInvoke(new Action(() =>
                {
                    List<UIElement> removeCandidates = new List<UIElement>();
                    foreach (UIElement ui in m_view.Children)
                    {
                        if (!(ui is Label))
                        {
                            removeCandidates.Add(ui);
                        }
                    }
                    foreach (var m in removeCandidates)
                    {
                        m_view.Children.Remove(m);
                    }
                    m_bridge = null;
                    Start(m_view);
                    OnStart();
                }));

        }

         private void ToStartPositions()
        {
            do
            {
                Thread.Sleep(2);
                m_view.Dispatcher.BeginInvoke(new Action(() =>
                {
                    m_island.MoveBack();
                    m_hero.MoveBack();
                    m_bridge.MoveBack();
                }));
            } while (m_island.OnStartPosition && !m_closing);
            m_view.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (m_hero.OnTheBridge)
                    {
                        m_hero.JumpDown();
                    }
                    double randomIslandWidth = m_island.SecondIslandWidth;
                    m_island.RemoveShapes();
                    m_bridge.RemoveShapes();
                    m_island = new Islands(m_view, randomIslandWidth);
                    m_bridge = null;
                }));
        }

         public void OnClosing()
         {
             m_closing = true;
             //Task.WaitAll(m_tasks.ToArray());
         }

        #endregion
    }

    #region IController
    #endregion

}
