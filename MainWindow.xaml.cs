﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StickHero
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Controller m_controller;


        public event Action OnStart = delegate { };
        public event Action OnUserMouseDown = delegate { };
        public event Action OnUserMouseUp = delegate { };
        public event Action OnFormClosing = delegate { };


        public MainWindow()
        {
            InitializeComponent();
            m_controller = new Controller(ViewField);

            m_controller.ScoreChanged += m_controller_ScoreChanged;
            OnStart += m_controller.OnStart;
            OnUserMouseDown += m_controller.OnMouseDown;
            OnUserMouseUp += m_controller.OnMouseUp;
            OnFormClosing += m_controller.OnClosing;
        }

        void m_controller_ScoreChanged(int Score, int BestScore)
        {
            ViewField.Dispatcher.BeginInvoke(new Action(() =>
                {
                    ScoreLabel.Content = Score;
                    BestScoreLabel.Content = BestScore;
                }));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ViewField.Children.Remove(StartButton);
            OnStart();
        }

        private void ViewFeald_MouseDown(object sender, MouseButtonEventArgs e)
        {
            OnUserMouseDown();
        }

        private void ViewFeald_MouseUp(object sender, MouseButtonEventArgs e)
        {
            OnUserMouseUp();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            OnFormClosing();
        }
    }
}
