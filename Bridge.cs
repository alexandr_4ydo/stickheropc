﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Threading;

namespace StickHero
{
    class Bridge
    {
        Rectangle m_bridgeRect;
        Canvas m_view;

        const double m_width = 6;
        const double m_ceiling = 50;
        double m_positionLeft = 75;
        double m_positionTop = 443;
        double m_height = 0;
        bool m_stopGrowth, m_lowered, m_isExist;


        public Bridge(Canvas View)
        {
            m_view = View;
            m_bridgeRect = new Rectangle();
            Draw();
            m_isExist = true;
        }

        #region Properties
        public bool IsExist
        {
            get { return m_isExist; }
        }

        public double Lenght
        {
            get
            {
                return m_positionLeft + m_height;
            }
        }
        #endregion

        #region Methods
        void Draw()
        {
            m_bridgeRect.Width = m_width;
            m_bridgeRect.Height = 1;
            m_bridgeRect.Fill = new SolidColorBrush(Colors.Black);
            m_bridgeRect.StrokeThickness = 1;
            Canvas.SetTop(m_bridgeRect, m_positionTop);
            Canvas.SetLeft(m_bridgeRect, m_positionLeft);
            m_view.Children.Add(m_bridgeRect);
        }

        public void StartGrow()
        {
            do
            {
                Thread.Sleep(2);
                
                m_positionTop--;

                m_view.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        m_height = m_bridgeRect.Height++;
                        Canvas.SetTop(m_bridgeRect, m_positionTop);
                    }));
            }
            while (m_positionTop != m_ceiling && !m_stopGrowth);
        }

        public void StopGrowth()
        {
            m_stopGrowth = true;
            if (!m_lowered && m_isExist)
            {
                m_lowered = true;                
            }
        }

        public void Lower()
        {
            int angle = 0;
            do
            {
                Thread.Sleep(2);
                angle++;
                m_view.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        RotateTransform rt = new RotateTransform();
                        rt.CenterX = 0;
                        rt.CenterY = m_bridgeRect.Height;
                        rt.Angle = angle;
                        m_bridgeRect.RenderTransform = rt;
                    }));
            }
            while (angle != 90);
        }

        public bool Hit(double IslandLeft, double IslandRight)
        {
            if (this.Lenght > IslandLeft && this.Lenght < IslandRight)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void RemoveShapes()
        {
            m_view.Children.Remove(m_bridgeRect);
        }

        public void MoveBack()
        {
            Canvas.SetLeft(m_bridgeRect, m_positionLeft--);
        }

        #endregion
    }
}
